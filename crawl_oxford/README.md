# Crawling Dictionary
Crawling content of essential dictionary

# How to run example
    - Clone project
    - Use commandline to go to the place where contains "spiders" directory
    - Run "scrapy crawl example" to see the result of crawler name "example" which crawl the (form, definition, examples)
# Future works
    - Create model class to save the crawled data and store to file
    - Edit code to crawl the content(definition, examples) of all important words and store it to json files or database 