# -*- coding: utf-8 -*-
import sys
sys.path.insert(0,"..")
import scrapy
from pprint import pprint
from scrapy.selector import HtmlXPathSelector
from lxml.html.soupparser import fromstring
from functools import reduce
from crawl_oxford.items import CrawlOxfordItem




class ExampleSpider(scrapy.Spider):
    name = 'example'
    #allowed_domains = ['en.oxforddictionaries.com']
    #start_urls = ['https://en.oxforddictionaries.com/definition/excited']
    
    allowed_domains = ['dictionary.cambridge.org', 'thesaurus.com']
    #start_urls = ['https://dictionary.cambridge.org/dictionary/english/{}']
    
     
    def __init__(self):
        self.URL = "https://dictionary.cambridge.org/dictionary/english/{}"
        self.URLThesaurus = "https://www.thesaurus.com/browse/{}"
        
        with open('20k_english_common_words.txt') as word_file:
            valid_words = set(word_file.read().split())
        
        #valid_words = ["hello", "man", "call"]
        self.words = valid_words
       

    def start_requests(self):
        # generate page IDs from 1000 down to 501
        for w in self.words:
            yield scrapy.Request(url = self.URL.format(w), callback=self.parse)

            
    def parse_synonym(self, response):
        item = response.meta['my_meta_item']
        try:
            holders = response.xpath('//ul[@class="css-1lc0dpe et6tpn80"]')
            synonym_holder = holders[0]
            antonym_holder = holders[1]
            
            #Parse synonyms
            synonyms = synonym_holder.xpath('.//li/span/a/text()').extract()
            antonyms = antonym_holder.xpath('.//li/span/a/text()').extract()
            
            item['synonyms'] = synonyms
            item['antonyms'] = antonyms
            yield item
      
        except Exception as e:
            item['synonyms'] = []
            item['antonyms'] = []
            yield item
            
        
   
    
    def parse(self, response):
        try :
            holders = response.xpath('//div[@class="entry-body__el clrd js-share-holder"]')
            results = []
            word = ""

            for h in holders:
                if word == "":
                    word = h.xpath('.//span[@class="hw"]/text()').extract_first()
                form = h.xpath('.//span[@class="posgram ico-bg"]//span[@class="pos"]/text()').extract_first()
                blocks = h.xpath('.//div[@class="def-block pad-indent"]')
                for b in blocks:

                    # Get definition
                    defi = b.xpath('.//b[@class="def"]').extract_first()
                    if (defi is None):
                        continue
                    defi_tree = fromstring(defi)
                    defi_elements = defi_tree.xpath(".//b/text() | //a/text()")
                    defi_sentence = reduce(lambda x, cul:  x + cul , defi_elements, '')
                    defi_sentence.replace("\n", " ")


                    # Get examples
                    emphasize_examples = b.xpath('.//span[@class="eg"]').extract()
                    examples_result = []
                    for emphasize_example in emphasize_examples:
                        emphasize_example_tree = fromstring(emphasize_example)
                        emphasize_example_elements = emphasize_example_tree.xpath('.//span/text() | //a/text()')
                        emphasize_example_sentence = reduce(lambda x, cul:  x + cul , emphasize_example_elements, '')
                        emphasize_example_sentence.replace("\n", " ")
                        emphasize_example_sentence = emphasize_example_sentence
                        examples_result.append(emphasize_example_sentence)

                    results.append((dict({"form": form, "definiton":defi_sentence, "examples": examples_result})))
            item = CrawlOxfordItem()
            item['word']= word
            item['meaning'] = results
            
          
            request_synonyms = scrapy.Request(url=self.URLThesaurus.format(word) , callback=self.parse_synonym)
            request_synonyms.meta['my_meta_item'] = item
           
            yield request_synonyms
            
           
        except Exception as e:
            print("Error:", e)
            item = CrawlOxfordItem()
            item['word']= ""
            item['meaning'] = ""
            yield item
            
                    
                    
                    
                    
            
            
            