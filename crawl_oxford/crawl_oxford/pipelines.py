# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
from scrapy.exporters import JsonItemExporter
from pymongo import MongoClient


class CrawlOxfordPipeline(object):
    def open_spider(self, spider):
        self.file = open('demofile.json', 'ab+')
        self.exporter = JsonItemExporter(self.file)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

        
    def __init__(self):
        pass
        # Create your database connection

    def process_item(self, item, spider): 
        self.exporter.export_item(item)
        print("This is word:>>>>>>>>>>>>>", str(item))   
        return item
    
class CambridgeDictionaryPipeline(object):
    def open_spider(self, spider):
        self.file = open('demofile.json', 'ab+')
        self.exporter = JsonItemExporter(self.file)
        self.exporter.start_exporting()

    def close_spider(self, spider):
        self.exporter.finish_exporting()
        self.file.close()

    def __init__(self):
       # Create your database connection
        self.client = MongoClient('104.198.200.184',
                             username='ngocthat',
                             password='thatnn@123',
                             authSource='admin'
                            )

        self.db = self.client.vc_article
        self.collection_dictionary = "article_dictionary"
        

    def process_item(self, item, spider):
        self.db[self.collection_dictionary].insert(dict(item), check_keys=False)
        print("Writing")
        #return item
       
        